// MundoServidor.h: interface for the CMundo class.
//Cabezera modificada. David Barcelo
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include <pthread.h>

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void NuevaEsfera();
	void RecibeComandosJugador();

	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;

	int puntos1;
	int puntos2;
	int timer;
	int fdfifologger;
	int fdfifocliente;
	int fdfifothread;
	char msj[50];
	char coordenadas[200];

	DatosMemCompartida MemComp;
	
	pthread_t thid1;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
