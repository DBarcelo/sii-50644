// Esfera.cpp: implementation of the Esfera class.
//Cabezera modificada. David Barcelo
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	timeReduce=0;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*t;
}

void Esfera::Reduce()
{
	if(timeReduce>150)
	{
		if (radio>0.2)radio-=0.05f;
		timeReduce=0;
		return;
	}
	timeReduce++;
}

