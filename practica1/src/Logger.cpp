#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//Lee del fichero cadena de texto

int main(int argc, char* argv[]) {
	
	//Se crean las variables locales	
	char readed[50];
	int fdfifo;
	int cierre = 0;
	
	//Se crea el fifo
	mkfifo("/tmp/FIFOLOG",0777);
	//Se abre el fichero
	fdfifo=open("/tmp/FIFOLOG", O_RDONLY);
	//Se comprueba si se ha abierto de forma correcta
	if(fdfifo == -1)
	{
		printf("Error al abrir el fifo");
		cierre =-1;
	}
	
	while(cierre != -1)
	{
		//Se lee el fifo
		cierre = read(fdfifo,readed,sizeof(readed));
		//Si cierre = -1 error en la lectura, si se lee '-' fin del juego, sino se imprime el resultado		
		if(cierre == -1)
			printf("\n Error al leer el fifo 1 \n");
		else if(readed[0]== '-')
		{
			printf("\n --FIN LOGGER-- \n");
			cierre=-1; 
		}
		else
			printf("%s\n", readed);
	}
	//Se cierra el descriptor
	close(fdfifo);
	//Se cierra el fifo
	unlink("/tmp/FIFOLOG");

	return -1;

}


