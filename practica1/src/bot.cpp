#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()

{

//Se crea el puntero a la memoria compartida y para la proyeccion
	DatosMemCompartida* pMemComp;
	int cierre = 0;

//Realizamos la apertura del fichero
	int fdbot;
	fdbot = open("/tmp/Botdata.txt",O_RDWR);
	if(fdbot == -1)
	{
		printf("Error al abrir el fichero Botdata");
		cierre =-7;
	}

//Se realiza la proyeccion el fichero
	pMemComp=(DatosMemCompartida*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,fdbot,0);
		pMemComp->end = 0;
//Despues de la proyeccion cerramos el fichero
	close(fdbot);


//Acciones de control de la raqueta
	while(cierre != -7)
	{
		usleep(25000);  
		float posRaqueta1;
		float posRaqueta2;
		posRaqueta1=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		posRaqueta2=((pMemComp->raqueta2.y2+pMemComp->raqueta2.y1)/2);

//En funcion de la posicion de la esfera moveremos la raqueta
		if(posRaqueta1 < pMemComp->esfera.centro.y)
			pMemComp->accion1 = 1;
		else if(posRaqueta1 > pMemComp->esfera.centro.y)
			pMemComp->accion1 = -1;
		else
			pMemComp->accion1 = 0;
		if(posRaqueta2<pMemComp->esfera.centro.y)
			pMemComp->accion2 = 1;
		else if(posRaqueta2>pMemComp->esfera.centro.y)
			pMemComp->accion2 = -1;
		else
			pMemComp->accion2 = 0;
		cierre = pMemComp->end;
	}

//Una vez terminado el bucle podremos desmontar la proyeccion
	munmap(pMemComp,sizeof(*(pMemComp)));

}
