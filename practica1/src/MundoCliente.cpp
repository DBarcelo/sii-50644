// Mundo.cpp: implementation of the CMundo class.
//Cabezera modificada. David Barcelo
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>

#include <sys/mman.h>
#include <fcntl.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

	//Se envia -7 para que bot cierre el mapeo
	pMemComp->end = -7;
	munmap(pMemComp,sizeof(*(pMemComp)));
	//Se cierra el fifo de comunicacion con server
	//Se cierra el descriptor
	close(fdfifocliente);
	//Se cierra el fifo
	unlink("/tmp/FIFOCLI");
	//Se cierra el fifo de comunicacion con thread
	//Se cierra el descriptor
	close(fdfifothread);
	//Se cierra el fifo
	unlink("/tmp/FIFOTHR");
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	MemComp.raqueta1.Dibuja();
	MemComp.raqueta2.Dibuja();	
	MemComp.esfera.Dibuja();
	

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
	int check;		
	check=read(fdfifocliente,coordenadas,sizeof(coordenadas));
	if(coordenadas[0]=='-' && coordenadas[1]=='-' )exit(1);
	sscanf(coordenadas,"%f %f %f %f %f %f %f %f %f %f %d %d", &MemComp.esfera.centro.x,&MemComp.esfera.centro.y,
		&MemComp.raqueta1.x1,&MemComp.raqueta1.y1,&MemComp.raqueta1.x2,&MemComp.raqueta1.y2,
		&MemComp.raqueta2.x1,&MemComp.raqueta2.y1,&MemComp.raqueta2.x2,&MemComp.raqueta2.y2,&puntos1,&puntos2);
	timer++;
	pMemComp->esfera=MemComp.esfera;
	pMemComp->raqueta1=MemComp.raqueta1;
	//En caso de un tiempo sin tocar ninguna tecla bot se hace cargo del jugador dos
	if(timer > 200) 
	{
		pMemComp->raqueta2=MemComp.raqueta2;
		switch(pMemComp->accion2)
		{

			case 1: 
				sprintf(tecla,"o");
				check = 0;		
				check=write(fdfifothread,tecla,sizeof(tecla));
				if((fdfifothread >=0) && (check != sizeof(tecla)))
				{
					perror("ERROR EN LA ESCRITURA ");
					exit(0);
				}			
				break;
			case 0: break;
			case -1: 
				sprintf(tecla,"l");
				check = 0;		
				check=write(fdfifothread,tecla,sizeof(tecla));
				if((fdfifothread >=0) && (check != sizeof(tecla)))
				{
					perror("ERROR EN LA ESCRITURA ");
					exit(0);
				}
				break;
		}
	}
//Precesamos la accion proyectada desde el bot.

	switch(pMemComp->accion1)
	{

		case 1: 
			sprintf(tecla,"w" );
			check = 0;		
			check=write(fdfifothread,tecla,sizeof(tecla));
			if((fdfifothread >=0) && (check != sizeof(tecla)))
			{
				perror("ERROR EN LA ESCRITURA ");
				exit(0);
			}			
			break;
		case 0: break;
		case -1: 
			sprintf(tecla,"s");
			check = 0;		
			check=write(fdfifothread,tecla,sizeof(tecla));
			if((fdfifothread >=0) && (check != sizeof(tecla)))
			{
				perror("ERROR EN LA ESCRITURA ");
				exit(0);
			}
			break;
	}

   }
void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{	int check = 0;
	switch(key)
	{

	case 's':
		sprintf(tecla,"s");
		check=0;	
		check=write(fdfifothread,tecla,sizeof(tecla));
		if((fdfifothread >=0) && (check != sizeof(tecla)))
		{
			perror("ERROR EN LA ESCRITURA ");
			exit(0);
		}
	break;
	case 'w':
		sprintf(tecla,"w");
		check = 0;		
		check=write(fdfifothread,tecla,sizeof(tecla));
		if((fdfifothread >=0) && (check != sizeof(tecla)))
		{
			perror("ERROR EN LA ESCRITURA ");
			exit(0);
		}
	break;
	case 'l':
		sprintf(tecla,"l");
		check = 0;		
		check=write(fdfifothread,tecla,sizeof(tecla));
		if((fdfifothread >=0) && (check != sizeof(tecla)))
		{
			perror("ERROR EN LA ESCRITURA ");
			exit(0);
		}
		timer=0;
	break;
	case 'o':
		sprintf(tecla,"o");
		check = 0;		
		check=write(fdfifothread,tecla,sizeof(tecla));
		if((fdfifothread >=0) && (check != sizeof(tecla)))
		{
			perror("ERROR EN LA ESCRITURA ");
			exit(0);
		}
	break;
	default:
	break;
	}
}

void CMundo::Init()
{
//temporizador
	timer=0;

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	MemComp.raqueta1.g=0;
	MemComp.raqueta1.x1=-6;MemComp.raqueta1.y1=-1;
	MemComp.raqueta1.x2=-6;MemComp.raqueta1.y2=1;

	//a la dcha
	MemComp.raqueta2.g=0;
	MemComp.raqueta2.x1=6;MemComp.raqueta2.y1=-1;
	MemComp.raqueta2.x2=6;MemComp.raqueta2.y2=1;
//Fichero proyectado en memoria
//Se Crea el fichero
	int fdfile=open("/tmp/Botdata.txt",O_RDWR|O_CREAT|O_TRUNC, 0777);
//Se escribe en el fichero
	int check = 0;
	check =write(fdfile,&MemComp,sizeof(MemComp)); 
	if((fdfile >=0) && (check != ((int)sizeof(MemComp))))
	{
		perror("ERROR EN LA ESCRITURA ");
		exit(0);
	}
//Se asigna al puntero de proyeccion el lugar donde esta proyectado el fichero
	pMemComp=(DatosMemCompartida*)mmap(NULL,sizeof(pMemComp),PROT_WRITE|PROT_READ,MAP_SHARED,fdfile,0);  
//Cerramos el fichero
	close(fdfile);  
//Inicializamos con accion 0: no mover
	pMemComp->end = 0;
	pMemComp->accion1=0; 
	pMemComp->accion2=0;
	//Se crea el fifo para comunicar con servidor
	mkfifo("/tmp/FIFOCLI",0777);
	//Se abre el fichero
	fdfifocliente=open("/tmp/FIFOCLI", O_RDONLY);
	//Se comprueba si se ha abierto de forma correcta
	if(fdfifocliente == -1)
	{
		printf("Error al abrir el fifo cliente ..");
	}
	//Se crea el fifo para comunicar con thread
	mkfifo("/tmp/FIFOTHR",0777);
	//Se abre el fichero
	fdfifothread=open("/tmp/FIFOTHR", O_WRONLY);
	//Se comprueba si se ha abierto de forma correcta
	if(fdfifothread == -1)
	{
		printf("Error al abrir el fifo thread ..");
	}
	
}

