// Mundo.cpp: implementation of the CMundo class.
//Cabezera modificada. David Barcelo
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>

#include <sys/mman.h>
#include <fcntl.h>

#include <signal.h>
void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            read(fdfifothread, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
	    
            if(key=='s')MemComp.raqueta1.velocidad.y=-4;
            if(key=='w')MemComp.raqueta1.velocidad.y=4;
            if(key=='l')MemComp.raqueta2.velocidad.y=-4;
            if(key=='o')MemComp.raqueta2.velocidad.y=4;
      }
}
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//Se envia el mensaje para cierre del fifo logger
	int check = 0;		
	sprintf(msj,"-");
	check=write(fdfifologger,msj,strlen(msj));
	if( (check == -1))
	{
		perror("Error en la escritura para cierre fifo");
		exit(0);
	}
	close(fdfifologger);
	//Se envia el mensaje para cierre del fifo cliente
	check = 0;		
	sprintf(msj,"---");
	check=write(fdfifocliente,msj,strlen(msj));
	if( (check == -1))
	{
		perror("Error en la escritura para cierre fifo");
		exit(0);
	}
	close(fdfifocliente);
	close(fdfifothread);
}
void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	MemComp.raqueta1.Dibuja();
	MemComp.raqueta2.Dibuja();	
	MemComp.esfera.Dibuja();
	

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	int i;
	int j;
	MemComp.raqueta1.Mueve(0.025f);
	MemComp.raqueta2.Mueve(0.025f);
	MemComp.esfera.Mueve(0.025f);

	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(MemComp.esfera);
		paredes[i].Rebota(MemComp.raqueta1);
		paredes[i].Rebota(MemComp.raqueta2);
	}
	
	MemComp.raqueta1.Rebota(MemComp.esfera);
	MemComp.raqueta2.Rebota(MemComp.esfera);
	if(fondo_izq.Rebota(MemComp.esfera))
	{
		MemComp.esfera.centro.x=0;
		MemComp.esfera.centro.y=rand()/(float)RAND_MAX;
		MemComp.esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		MemComp.esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(msj,"Jugador 2 marca 1 punto, lleva %d", puntos2);
		int check = 0;			
		check=write(fdfifologger,msj,strlen(msj)+1);
		if((fdfifologger >=0) && (check != (int)(strlen(msj)+1)))
		{
			perror("ERROR EN LA ESCRITURA ");
			exit(0);
		}
		
	}
	if(fondo_dcho.Rebota(MemComp.esfera))
	{
		MemComp.esfera.centro.x=0;
		MemComp.esfera.centro.y=rand()/(float)RAND_MAX;
		MemComp.esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		MemComp.esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(msj,"Jugador 1 marca 1 punto, lleva %d", puntos1);
		int check = 0;			
		check=write(fdfifologger,msj,strlen(msj)+1);
		if(fdfifologger && (check != (int)(strlen(msj)+1)))
		{
			perror("ERROR EN LA ESCRITURA ");
			exit(0);
		}
	}

// implementamos si alguno de los jugadores llega a 3 puntos gana la partida
	if (puntos2==3) 
	{
		sprintf(msj,"Jugador 2 con %d puntos gana la partida.", puntos2);
		int check;		
		check=write(fdfifologger,msj,strlen(msj)+1);
		if((fdfifologger >=0) && (check != (int)(strlen(msj)+1)))
		{
			perror("ERROR EN LA ESCRITURA ");
			exit(0);
		}
		exit (0);	
	}
	else if(puntos1==3) 
	{
		sprintf(msj,"Jugador 1 con %d puntos gana la partida.", puntos1);
		int check = 0;		
		check=write(fdfifologger,msj,strlen(msj)+1);
		if((fdfifologger >=0) && (check != (int)(strlen(msj)+1)))
		{
			perror("ERROR EN LA ESCRITURA ");
			exit(0);
		}
			exit(0);
	}
	sprintf(coordenadas,"%f %f %f %f %f %f %f %f %f %f %d %d",MemComp.esfera.centro.x,MemComp.esfera.centro.y,
		MemComp.raqueta1.x1,MemComp.raqueta1.y1,MemComp.raqueta1.x2,MemComp.raqueta1.y2,
		MemComp.raqueta2.x1,MemComp.raqueta2.y1,MemComp.raqueta2.x2,MemComp.raqueta2.y2,puntos1,puntos2);
	int check;		
	check=write(fdfifocliente,coordenadas,sizeof(coordenadas));
	if((fdfifocliente >=0) && (check != sizeof(coordenadas)))
	{
		perror("ERROR EN LA ESCRITURA  del fifo coordenadas");
		//exit(0);
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{

	case 's':MemComp.raqueta1.velocidad.y=-4;break;
	case 'w':MemComp.raqueta1.velocidad.y=4;break;
	case 'l':
		MemComp.raqueta2.velocidad.y=-4;
		timer=0;
		break;
	case 'o':
		MemComp.raqueta2.velocidad.y=4;
		timer=0;
		break;

	}
}

void CMundo::Init()
{
//temporizador
	timer=0;

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	MemComp.raqueta1.g=0;
	MemComp.raqueta1.x1=-6;MemComp.raqueta1.y1=-1;
	MemComp.raqueta1.x2=-6;MemComp.raqueta1.y2=1;

	//a la dcha
	MemComp.raqueta2.g=0;
	MemComp.raqueta2.x1=6;MemComp.raqueta2.y1=-1;
	MemComp.raqueta2.x2=6;MemComp.raqueta2.y2=1;
	//Apertura del fifo para logger
	fdfifologger=open("/tmp/FIFOLOG",O_WRONLY);
	//Apertura del fifo para cliente
	fdfifocliente=open("/tmp/FIFOCLI",O_WRONLY);
	//Apertura del fifo del thread
	pthread_create(&thid1, NULL, hilo_comandos, this);
	fdfifothread=open("/tmp/FIFOTHR",O_RDONLY);

}

